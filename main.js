let inputPrice = document.querySelector('#price');
inputPrice.addEventListener('focus', function () {
   inputPrice.style.border =  "none";
   inputPrice.style.border =  "4px solid #006400";
});

let currentPrice = document.createElement('span');
let textError = document.querySelector('h1');

inputPrice.addEventListener('blur',() => {
   if(inputPrice.value <= 0) {
      inputPrice.style.border = "4px solid #FF0000";
      textError.innerText = "Please enter correct price.";
   }
      else{
         textError.remove();
         document.querySelector('label').before(currentPrice);
         currentPrice.innerHTML = `Current Price= ${inputPrice.value} <button class="close">+</button>`;
         let close = document.querySelector('.close');
         inputPrice.style.color= "#008000";
         close.addEventListener('click', () => {
            currentPrice.remove();
            inputPrice.value= "0";
         });
   }
});
